# Import packages
import argparse
from time import sleep
import json
import paho.mqtt.client as mqtt

# Import custom packages
import tf_utility
from timeout_utility import *
from cv2_utility import turn_on_cam, take_picture, stop_camera

Tracking = True # State if tracking is active
webcam = None # CV webcam object

# Global person counter vars
curr_person_count = None
person_count = 0
times_person_count_same = 0


## Utility functions
def exit_script(exit_code):
    global webcam
    stop_camera(webcam)
    exit(exit_code)

## Setup
# Define and parse input arguments
parser = argparse.ArgumentParser()
parser.add_argument('--modeldir', help='Folder the .tflite file is located in',
                    required=False, default=".")
parser.add_argument('--graph', help='Name of the .tflite file, if different than detect.tflite',
                    default='detect.tflite')
parser.add_argument('--labels', help='Name of the labelmap file, if different than labelmap.txt',
                    default='labelmap.txt')
parser.add_argument('--threshold', help='Minimum confidence threshold for displaying detected objects',
                    default=0.6)
parser.add_argument('--resolution', help='Desired webcam resolution in WxH. If the webcam does not support the resolution entered, errors may occur.',
                    default='1280x720')

with open("config.json") as conf:
    config = json.load(conf)

# mqtt connection
def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("[INFO] Connected with result code " + str(rc))

        print("[INFO] Subscribing to topic",(config["mqttTopic"] + "/cmd"))
        client.subscribe((config["mqttTopic"] + "/cmd"))
    else:
        print("[ERROR] Bad connection with result code " + str(rc))
        client.loop_stop()
        exit_script(1)

def on_message(client, userdata, message):
    global Tracking

    print("[INFO] message received " ,str(message.payload.decode("utf-8")))
    print("[INFO] message topic=",message.topic)
    print("[INFO] message qos=",message.qos)
    print("[INFO] message retain flag=",message.retain)
    
    if (str(message.payload.decode("utf-8")) == "off"):
        Tracking = False
    if (str(message.payload.decode("utf-8")) == "on"):
        # reset timeout on message
        resetTimeout()
        # start Tracking
        Tracking = True

def on_disconnect(client, userdata, rc):
    print("[INFO] Disconnected with result code " + str(rc))

client = mqtt.Client(config["mqttClient"])
client.username_pw_set(username=config["mqttUser"], password=config["mqttPass"])
client.on_connect = on_connect
client.on_disconnect = on_disconnect
client.on_message=on_message # attach function to callback

try:
    client.connect(config["mqttServer"], 1883, 60)
except:
    print("Error: MQTT connection failed")
    exit_script(1)

client.loop_start()

# setup TF model
tf_utility.setup(parser)

# setup Timeout utility
setTimeoutLimit(config["timeout_time"])
setOffLimit(config["delay_til_off"])

# smoothing function for Person Counter
# returns true if person counter has been the same for three consecutive times
def smoothCount(count):
    global person_count, times_person_count_same
    try:
        if count == person_count and times_person_count_same >= 2: # if same count for 3 times return count
            print("[INFO] new Person Count: ", count)
            return True
        elif count == person_count:                                 # if same count but less that 3 times add 1
            times_person_count_same += 1
        else:
            person_count = count                                    # if not same count set curr count and reset times
            times_person_count_same = 0
        return False
    except Exception as e:
        print("[ERRO] ", e)

## main loop
while True:
    webcam = turn_on_cam(webcam)
    while Tracking:
        picture = take_picture(webcam)
        detected, person_score, detected_objects, person_count = tf_utility.found_person(picture)

        if (smoothCount(person_count)):
            curr_person_count = person_count

        # publish occupied on detection
        if detected:
            print("[INFO] probability: " + str(person_score))
            print("[INFO] publishing occupied")
            # Create JSON message
            message = {
                "status": "Occupied",
                "person_score": person_score,
                "detected_objects": detected_objects,
                "person_count": curr_person_count
            }
            # Convert detected_objects to a JSON string
            message["detected_objects"] = json.dumps(detected_objects)
            # Publish message as JSON
            client.publish(config["mqttTopic"], json.dumps(message))
            resetTimeout()
        # publish unoccupied after timeout when not detected
        elif not isinstance(picture, type(None)):
            if ckeckOffLimit():
                print("[INFO] publishing unoccupied")
                # Create JSON message
                message = {
                    "status": "Unoccupied",
                    "person_score": person_score,
                    "detected_objects": detected_objects,
                    "person_count": 0
                }
                # Convert detected_objects to a JSON string
                message["detected_objects"] = json.dumps(detected_objects)
                # Publish message as JSON
                client.publish(config["mqttTopic"], json.dumps(message))
            addToTimeout(2)
        # skip on no picture
        if isinstance(picture, type(None)):
            print("[INFO] no picture")
        # stop tracking on timeout
        if checkForTimeout():
            Tracking = False
            resetTimeout()
            break
        sleep(2)

    stop_camera(webcam)
    sleep(2)
