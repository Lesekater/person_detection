# Person_detection
https://codeberg.org/Lesekater/person_detection

A script that checks if a person is detected in a webcam-feed and sends a JSON message containing
- the status (occupied/unoccupied)
- the probability of a person being detected
- a list of detected objects
- the number of persons detected

to an mqtt topic.

Works with [Home Assistant](https://www.home-assistant.io/) (See [Config Example](#home-assistant-config-example))

## Description

### `person_detection_lite.py`
The Script acceses the local webcam and captures frames at an interval. These Frames are then evaluated by a ml model throught cv2. Based on the result it sends a json state to an mqtt topic.

The Script can be paused and resumed by sending ether "off" or "on" to the _configuredMqttTopic_/cmd.

## Installation

**Ubuntu**: A version of Python is already installed.  
**Mac OS X**: A version of Python is already installed.  
**Windows**: You will need to install one of the 3.x versions available at [python.org](http://www.python.org/getit/). (SCRIPT IS NOT TESTED!!)

## General usage information

1. Download the script.
2. Install the Dependencies. (pip install -r requirements.txt)
3. Configure the Script by creating an config.json (see configExample.json)
4. Run the script. (python3 person_detection_lite.py)

## Home Assistant Config Example

```yaml
binary_sensor:
  - platform: mqtt
    name: "Person Detection"
    state_topic: "exampleTopic/Subtopic"
    payload_on: "Occupied"
    payload_off: "Unoccupied"
    device_class: occupancy
    value_template: "{{ value_json.status }}"
    json_attributes_topic: "exampleTopic/Subtopic"
    json_attributes_template: "{{ value_json | tojson }}"
    expire_after: 10
sensor:
  - platform: mqtt
    name: "Person Detection Probability"
    state_topic: "exampleTopic/Subtopic"
    value_template: "{{ value_json.person_score | float | round(2) }}"
    unit_of_measurement: "%"
    device_class: probability
    json_attributes_topic: "exampleTopic/Subtopic"
    json_attributes_template: "{{ value_json | tojson }}"
    expire_after: 10
  - platform: mqtt
    name: "Person Count"
    state_topic: "exampleTopic/Subtopic"
    value_template: "{{ value_json.person_count }}"
    device_class: total
    json_attributes_topic: "exampleTopic/Subtopic"
    json_attributes_template: "{{ value_json | tojson }}"
    expire_after: 10
```
