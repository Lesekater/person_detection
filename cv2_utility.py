# Import packages
import cv2

# function to loop over the first 10 connected cameras and create a cv2 Videostream
def turn_on_cam(camera):
    if camera != None: 
        print("[Warning] Tried to open webcam twice")
        return
    for i in range(10):
        # startup webcam
        print("[INFO] starting up webcam..")
        camera = cv2.VideoCapture(i)
        if camera.isOpened():
            break
    # catch no camera available error
    if camera is None or not camera.isOpened():
        print("[Error] unable to open webcam")
        exit(1)
    return camera

# function to take a picture from a cv2 Videostream
# returns None if picture fails
def take_picture(camera):
    ret, frame = camera.read()

    if not ret:
        return None

    return frame

def stop_camera(camera):
    if camera != None and not camera.isOpened():
        camera.release()