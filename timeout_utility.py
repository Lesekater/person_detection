time_since_last_detection = 0
off_limit = 20
timeout_limit = 120

def resetTimeout():
    global time_since_last_detection
    time_since_last_detection = 0

def addToTimeout(added_time):
    global time_since_last_detection
    time_since_last_detection += added_time

def setTimeoutLimit(limit):
    global timeout_limit
    timeout_limit = limit

def setOffLimit(limit):
    global off_limit
    off_limit = limit

def ckeckOffLimit():
    global time_since_last_detection, off_limit
    return time_since_last_detection >= off_limit

def checkForTimeout():
    global time_since_last_detection, timeout_limit
    return time_since_last_detection >= timeout_limit